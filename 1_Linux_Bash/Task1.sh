#!/bin/bash
tonum() {
    if [[ $1 =~ ([[:digit:]]+)\.([[:digit:]]+)\.([[:digit:]]+)\.([[:digit:]]+) ]]; then
        addr=$(( (${BASH_REMATCH[1]} << 24) + (${BASH_REMATCH[2]} << 16) + (${BASH_REMATCH[3]} << 8) + ${BASH_REMATCH[4]} ))
        eval "$2=\$addr"
    fi
}
toaddr() {
    b1=$(( ($1 & 0xFF000000) >> 24))
    b2=$(( ($1 & 0xFF0000) >> 16))
    b3=$(( ($1 & 0xFF00) >> 8))
    b4=$(( $1 & 0xFF ))
    eval "$2=\$b1.\$b2.\$b3.\$b4"
}
showAll()
{
   nmap_installed=1
   if ! command -v nmap &> /dev/null
   then
       echo "No NMAP found use alternative way"
       nmap_installed=0
   fi
   IFS=$'\n'
   ip_addresses=( $(ip addr show | awk '/inet / {print $2}') )
   IFS=$' '
   for i in "${ip_addresses[@]}"
   do
   	ip_and_mask=( $(echo $i | awk  -F'/' '{print $1, $2}' ) )
        IPADDR=${ip_and_mask[0]}
	if [ $IPADDR == "127.0.0.1" ]
   	then 
   	  echo "Skipping localhost - $IPADDR"
   	  continue
   	fi
   	if [ $nmap_installed -eq 1 ]
   	then
   	    nmap -sn $i | awk '/Nmap scan report/{print $NF}'
   	    continue
   	fi
        echo "Broadcast ping for interface $IPADDR Please wait"
    	NETMASKLEN=${ip_and_mask[1]}
    	zeros=$((32-NETMASKLEN))
    	NETMASKNUM=0
    	for (( i=0; i<$zeros; i++ )); do
        	NETMASKNUM=$(( (NETMASKNUM << 1) ^ 1 ))
    	done
    	NETMASKNUM=$((NETMASKNUM ^ 0xFFFFFFFF))
    	toaddr $NETMASKNUM NETMASK
    	tonum $IPADDR IPADDRNUM
	tonum $NETMASK NETMASKNUM
	INVNETMASKNUM=$(( 0xFFFFFFFF ^ NETMASKNUM ))
	NETWORKNUM=$(( IPADDRNUM & NETMASKNUM ))
	BROADCASTNUM=$(( INVNETMASKNUM | NETWORKNUM ))
	toaddr $BROADCASTNUM BROADCAST
	ping -b $BROADCAST > /dev/null
   done
   if [ $nmap_installed==0 ]
   	then
   	echo $(arp -a) | awk '{print $2}'
  fi
}
showOpenPorts()
{
   #NAMP is not always installed by default
   nc -zv 192.168.0.1 1-65535 2>&1 | awk '/succeeded/ {print $4," - IS OPEN"}'
}
showErrorParametrAndExit()
{
  echo "Select paramter
  	--all key displays the IP addresses and symbolic names of all hosts in the current subnet 
  	--target [IP] -  key displays a list of open system TCP ports."
  exit 1;
 }
if [ -z $1 ]
then
  showErrorParametr	
fi
case $1 in
	"--all")
	  showAll
	;;
	"--target")
	  if [ -z $2 ]
	  then
	    echo "No IP specified"
	    showErrorParametrAndExit
	  fi
	  showOpenPorts
	;;
	*)
	  echo "Wrong parameter!"
	  showErrorParametrAndExit
	;;
esac

