#!/bin/bash
if [ $# -ne 2 ]
   then
   echo "Error in arguments. Use <script> [source dir] [dest dir]"
   exit 1
fi 
rsync -avu --delete  --out-format="%t %f %o" $1 $2 | sed '1,2d;$d' | sed '$d' | awk NF >> log.txt

